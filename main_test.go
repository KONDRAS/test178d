// Task178d project main.go

package main

import (
	"io"
	"reflect"
	"testing"
)

func Test_main(t *testing.T) {
	tests := []struct {
		name string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			main()
		})
	}
}

func Test_check(t *testing.T) {
	type args struct {
		e error
	}
	tests := []struct {
		name string
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			check(tt.args.e)
		})
	}
}

func TestReadMyData(t *testing.T) {
	type args struct {
		argsProg []string
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ReadMyData(tt.args.argsProg); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadMyData() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReadInts(t *testing.T) {
	type args struct {
		r io.Reader
	}
	tests := []struct {
		name    string
		args    args
		want    []int
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := ReadInts(tt.args.r)
			if (err != nil) != tt.wantErr {
				t.Errorf("ReadInts() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ReadInts() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFindingQuantity(t *testing.T) {
	type args struct {
		array []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := FindingQuantity(tt.args.array); got != tt.want {
				t.Errorf("FindingQuantity() = %v, want %v", got, tt.want)
			}
		})
	}
}
