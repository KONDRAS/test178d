// Task178d project main.go
//Finding quantity of natural numbers with condition 2k< ak < k!;
package main

import (
	"bufio"
	"fmt"
	"io"
	"math/rand"
	"os"
	"strconv"
)

var natrow []int

func check(e error) {
	if e != nil {
		panic(e)
	}
}
func main() {
	argsProg := os.Args[1:]
	natrow := ReadMyData(argsProg)
	fmt.Println("Row of Ak is ", natrow)
	fmt.Println("Quantity of 2^k < Ak < k! is :", FindingQuantity(natrow))
}

func ReadMyData(argsProg []string) []int {

	var n int
	var err error

	if len(argsProg) == 0 { // input without parameters
		fmt.Print("Enter number n:")
		fmt.Scanln(&n)
		for i := 1; i <= n; i++ {
			natrow = append(natrow, rand.Intn(1000))
		}
	} else {
		if len(argsProg) == 1 { //if input is file name
			filename := argsProg[0]
			f, err := os.Create(filename)
			check(err)
			r := io.Reader(f)
			natrow, err = ReadInts(r)
			check(err)
		} else if len(argsProg) > 1 { //input od array of natural numers
			for i := range argsProg {
				ss, err := strconv.Atoi(argsProg[i])
				natrow = append(natrow, ss)
				check(err)
			}
		}
	}
	_ = err

	return natrow
}

func ReadInts(r io.Reader) ([]int, error) {
	scanner := bufio.NewScanner(r)
	scanner.Split(bufio.ScanWords)
	var result []int
	for scanner.Scan() {
		x, err := strconv.Atoi(scanner.Text())
		if err != nil {
			return result, err
		}
		result = append(result, x)
	}
	return result, scanner.Err()
}

func FindingQuantity(array []int) int {

	k := 0
	fac := 1

	for i := 1; i < len(array); i++ {
		fac = fac * i
		//		fmt.Println(2<<i, "___", fac)
		if array[i-1] > 2<<i && array[i-1] < fac {
			k++
		}
	}
	return k
}
